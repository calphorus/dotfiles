#!/usr/bin/env bash

# Get script location
SCRIPT_DIR="$(cd "$(dirname ${BASH_SOURCE[0]})" && pwd)"

################################################################################
# Install zsh dotfiles
#

source "$SCRIPT_DIR/zsh/zsh-install.sh"

################################################################################
# Install neovim stuff

source "$SCRIPT_DIR/neovim/neovim-install.sh"

################################################################################
# Install qutebrowser stuff

source "$SCRIPT_DIR/qutebrowser/qutebrowser-install.sh"
