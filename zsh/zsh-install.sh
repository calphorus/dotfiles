#!/usr/bin/env bash

# Is symlink? Delete.
if [[ -L "$HOME/.zshrc" ]]; then
	rm "$HOME/.zshrc"
else
    # Is file? Move.
	if [[ -f "$HOME/.zshrc" ]]; then
		mv "$HOME/.zshrc" "$HOME/.zshrc.bak"
	fi
fi

# Is symlink? Delete.
if [[ -L "$HOME/.zshenv" ]]; then
    rm "$HOME/.zshenv"
else
    # Is file? Move.
    if [[ -f "$HOME/.zshenv" ]]; then
        mv "$HOME/.zshenv" "$HOME/.zshenv.bak"
    fi
fi

# Make zsh directory
if [[ ! -d "$HOME/.zsh" ]]; then
    mkdir -p "$HOME/.zsh"
fi

if [[ -a "$HOME/.zsh/calphorus.zsh-theme" ]]; then
	# Previously installed version? Delete.
	rm "$HOME/.zsh/calphorus.zsh-theme"
fi

ln -s "$SCRIPT_DIR/zsh/zshrc" "$HOME/.zsh/.zshrc"
ln -s "$SCRIPT_DIR/zsh/zshenv" "$HOME/.zshenv"
ln -s "$SCRIPT_DIR/zsh/agnoster/agnoster.zsh-theme" "$HOME/.zsh/calphorus.zsh-theme"


