" Plugins

call plug#begin( '~/.local/share/nvim/plugged' )
    Plug 'calphorus/vim-gitgutter'
    Plug 'calphorus/vim-multiple-cursors'
    Plug 'calphorus/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
    Plug 'calphorus/fzf.vim'
    Plug 'calphorus/vim-easymotion'
    Plug 'calphorus/onedark.vim'
    Plug 'calphorus/editorconfig-vim'
    Plug 'calphorus/nuake'
call plug#end()

" Base settings

set smartcase                   " Enable smart-case searches
set ignorecase                  " Case-insensitive searches.
set incsearch                   " Always search for strings incrementally.
set cursorline                  " Highlight line cursor is on.
set number                      " Display numbers on left side.
set tabstop=4                   " Number of spaces per tab.
set shiftwidth=4                " Number of spaces to indent.
set expandtab                   " Spaces, not tabs.
set updatetime=100              " Autosave swap file after this amount of time (if nothing is typed).
set autoindent                  " Auto indent new lines.
set cindent                     " Use c-style indentations.
set ruler                       " Show line and column information.
set autoread                    " Reload buffer modified outside of Neovim.
set showcmd                     " Show command in status bar.
set undolevels=9999999999       " Number of undo levels.
set backspace=indent,eol,start  " Backspace behaviour.
set spell                       " Set spell-check enabled by default.

syntax on
colorscheme onedark

" Variables

let s:panel_count = 1

" Automatic toggles

augroup NumberToggle
    autocmd!
    autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
    autocmd BufLeave,FocusLost,InsertEnter * set norelativenumber
augroup END

" Always jump to the last known cursor position.
" Don't do it when the position is invalid, or
" inside an event handler.

augroup CursorPos
    autocmd BufReadPost *
        \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
        \ | exe "normal! g`\""
        \ | endif
augroup END

" Functions that provide useful information about the system, or perform
" an action that causes something useful to happen.

" Check if the user is running on battery power (this is mostly for laptops).
function! AmIOnBattery()
    if has( 'unix' )
        return readfile('/sys/class/power_supply/AC/online') == ['0']
    else
        return false
    endif
endfunction()

" Toggles whether or not spell-check is enabled.
function! ToggleSpellCheck()
    if( 1==&spell )
        set nospell
    else
        set spell
    endif
endfunction

" Smooth scroll up or down in the buffer
function! SmoothScroll(up)
    let scrollaction=""
    let l:sleeptime=2
    if a:up
        let scrollaction=""
        let l:sleeptime=10
    endif
    exec "normal! " . scrollaction
    redraw
    let counter=1
    let count=&scroll
    while counter < count
        let counter+=1
        exec "sleep " . l:sleeptime . "m"
        exec "normal! " . scrollaction
        redraw
    endwhile
endfunction

" Closes all other windows but the currently selected window
function CloseOtherWindows()
    only
    let s:panel_count = 1
endfunction

" Performs a vertical split only if there is one panel.
function VerticalSplit()
    if 2 == s:panel_count
        return
    endif
    " Ensure splitting properly
    set splitright
    " Make the split
    execute 'vs'
    let s:panel_count += 1
endfunction

" Swaps windows
function SwapWindows()
    if 2 == s:panel_count
        " Get current buffer
        let l:current_buffer = bufnr('%')
        let l:current_window = winnr()

        if l:current_window == winnr('$') && 0 != l:current_window
            let l:other_window = l:current_window - 1
        else
            let l:other_window = l:current_window + 1
        endif

        " Switch to other window
        exec l:other_window . 'wincmd w'

        " Get the other buffer
        let l:other_buffer = bufnr('%')

        " Hide and open buffer
        exe 'hide buf' . l:current_buffer

        " Switch back to previous window
        exec l:current_window . 'wincmd w'
        " Hide and open buffer
        exec 'hide buf' . l:other_buffer
    endif
endfunction

" Returns the expected extensions for C and C++ source files
function GetCAndCXXSourceExts()
    " *.inl files are included in this list as they are the implementing files
    " for some header files, even though .inl files are also headers themselves.
    return ['c', 'cpp', 'cxx', 'cc', 'inl']
endfunction

" Returns the expected extensions for C and C++ header files
function GetCAndCXXHeaderExts()
    return ['h', 'hpp']
endfunction

" Determine if the current buffer is a C or C++ source/header file. Returns 1 if
" the file is a C or C++ file; otherwise, returns 0.
function IsCOrCXXFile()
    " Get the extension of the buffer
    let extension = expand('%:e')
    " The expected extensions for header and source files
    let source_exts = GetCAndCXXSourceExts()
    let header_exts = GetCAndCXXHeaderExts()

    " Determine if the buffer is a source file
    for source_ext in source_exts
        if extension == source_ext
            return 1
        endif
    endfor

    " Determine if the buffer is a header file
    for header_ext in header_exts
        if extension == header_ext
            return 1
        endif
    endfor

    " Not a recognized C or C++ file.
    return 0
endfunction

" Determine if the current buffer is a C or C++ header file. Returns 1 if the
" file is a C or C++ header file; otherwise, returns 0.
function IsCOrCXXHeaderFile()
    " Get the extension of the buffer
    let extension = expand('%:e')
    " The expected extensions for header files
    let header_exts = GetCAndCXXHeaderExts()

    for header_ext in header_exts
        if extension == header_ext
            return 1
        endif
    endfor

    " Not a recognized C or C++ header file
    return 0
endfunction

" Determine if the current buffer is a C or C++ source file. Returns 1 if the
" file is a C or C++ source file; otherwise, returns 0.
function IsCOrCXXSourceFile()
    " Get the extension of the buffer
    let extension = expand('%:e')
    " The expected extensions for source files
    let source_exts = GetCAndCXXSourceExts()

    for source_ext in source_exts
        if extension == source_ext
            return 1
        endif
    endfor

    " Not a recognized C or C++ source file
    return 0
endfunction

" Find matching header file for a C or C++ source file. Returns the path of the
" matching file; or an empty string if not found.
function GetMatchingCOrCXXHeaderFile()
    let current_file = expand('%:t:r')
    let current_buf  = bufnr('%')
    let header_exts  = GetCAndCXXHeaderExts()

    let all_buffers  = range(1, bufnr('$'))

    for buffer_number in all_buffers
        if buffer_number != current_buf
            let buffer_name  = bufname(buffer_number)
            let buffer_ext   = fnamemodify(buffer_name, ':e')
            let buffer_file  = fnamemodify(buffer_name, ':t:r')

            if buffer_file == current_file
                for header_ext in header_exts
                    if buffer_ext == header_ext
                        return buffer_name
                    endif
                endfor
            endif
        endif
    endfor

    " Not found!
    return ""
endfunction

" Find matching source file for a C or C++ header file. Returns the path of the
" matching file; or an empty string if not found.
function GetMatchingCOrCXXSourceFile()
    let current_file = expand('%:t:r')
    let current_buf  = bufnr('%')
    let source_exts  = GetCAndCXXSourceExts()

    let all_buffers  = range(1, bufnr('$'))

    for buffer_number in all_buffers
        if buffer_number != current_buf
            let buffer_name  = bufname(buffer_number)
            let buffer_ext   = fnamemodify(buffer_name, ':e')
            let buffer_file  = fnamemodify(buffer_name, ':t:r')

            if buffer_file == current_file
                for source_ext in source_exts
                    if buffer_ext == source_ext
                        return buffer_name
                    endif
                endfor
            endif
        endif
    endfor

    " Not found!
    return ""
endfunction

" If a C or C++ file is the current buffer, attempt to find the associated file
" for the buffer (if the buffer is a header file, attempt to find the source
" file with the same name and vice versa).
function GetMatchingCOrCXXFile()
    " If the current buffer is not a C or C++ file, return empty.
    if 0 == IsCOrCXXFile()
        return ""
    endif

    if IsCOrCXXSourceFile()
        return GetMatchingCOrCXXHeaderFile()
    endif

    " Must be a header file
    return GetMatchingCOrCXXSourceFile()
endfunction

" Gets the matching file for the current C or C++ buffer, and creates a vertical
" split to the left of the current window containing the matching file. If there
" are two windows open already, swaps the current buffer to the other window and
" swaps the other buffer with the matching buffer.
function SearchAndSplitVert(filename)
    if(empty(glob(a:filename)) || a:filename == bufname('%'))
        " Do nothing for invalid filenames or files that have not been loaded
        return
    endif

    if 2 == s:panel_count
        call SwapWindows()
        " Hide and open buffer
        exec 'hide buf' . bufnr(a:filename)
    else
        " Ensure it splits to the left
        set nosplitright
        execute 'vert sf ' . a:filename
        " Resume normal behaviour
        set splitright

        let s:panel_count += 1
    endif
endfunction

" Key bindings.
"
" These are a little special, as I have to change up a bunch of the default
" settings due to using the Colemak keyboard layout. The default bindings
" really do not like non-QWERTY users it seems.

nnoremap <SPACE> <Nop>
let mapleader=" "

" Map ; to fzf's :Files command
nnoremap ; :Files<CR>

" Replace colon key
nnoremap <silent> p :

" Movement keys in normal mode
nnoremap <silent> j <C-Left>
nnoremap <silent> J <Left>
nnoremap <silent> k <Down>
nnoremap <silent> K }
nnoremap <silent> l <Up>
nnoremap <silent> L {
nnoremap <silent> ; <C-Right>
nnoremap <silent> : <Right>
nnoremap <expr> <silent> n col('.') == match(getline('.'),'\S')+1 ? '0' : '^'
nnoremap <silent> m $
nnoremap <silent> <C-i> :call SmoothScroll(1)<CR>
nnoremap <silent> <C-o> :call SmoothScroll(0)<CR>

" Movement keys between windows
nnoremap <silent><M-u> <C-w>h
nnoremap <silent><M-i> <C-w>j
nnoremap <silent><M-o> <C-w>k
nnoremap <silent><M-p> <C-w>l

" Clear higlight and redraw
nnoremap <silent><C-l> :nohlsearch<CR>:normal! <CR>

" Escape key for :terminal
tnoremap <Esc> <C-\><C-n>

" SAVE!!!!!
nnoremap <silent> w :write<CR>

" Swap windows
nnoremap <silent><Leader>k :call SwapWindows()<CR>

" Open new panel
nnoremap <silent>vs :call VerticalSplit()<CR>

" Open matching C or C++ file
nnoremap <silent><Leader>c :call SearchAndSplitVert(GetMatchingCOrCXXFile())<CR>

" Close other windows
nnoremap <silent><Leader>w :call CloseOtherWindows()<CR>

" Find and replace
nnoremap <Leader>r :%s//g<Left><Left>

" Next in search
nnoremap <silent>s /<CR>

" Toggle spell check
nnoremap <silent><Leader>s :call ToggleSpellCheck()<CR>

" Open and close a Nuake window
nnoremap <silent> <C-e> :Nuake<CR>
tnoremap <silent> <C-e> <C-\><C-n>:Nuake<CR>

" Nuake settings

let g:nuake_position = 'top'
let g:nuake_size = 0.3
let g:nuake_per_tab = 0
let g:close_if_last_standing = 1
let g:start_insert = 1

