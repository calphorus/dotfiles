@SETLOCAL
@ECHO OFF

@REM Install Neovim configurations

@REM Ensure that Neovim directory exists for init.vim.
IF NOT EXIST %USERPROFILE%\AppData\Local\nvim (
    MKDIR %USERPROFILE%\AppData\Local\nvim
)

@REM Create autoload directory for plug.vim
IF NOT EXIST %USERPROFILE%\AppData\Local\nvim\autoload (
    MKDIR %USERPROFILE%\AppData\Local\nvim\autoload
)

@REM Install the Neovim config.
IF NOT EXIST %USERPROFILE%\AppData\Local\nvim\init.vim (
    MKLINK %USERPROFILE%\AppData\Local\nvim\init.vim %CD%\init.vim
)

@REM Download and install Vim-Plug
POWERSHELL -Command "(new-object System.Net.WebClient).DownloadFile('https://gitlab.com/calphorus/vim-plug/raw/master/plug.vim', '%USERPROFILE%/AppData/Local/nvim/autoload/plug.vim')"
