#!/usr/bin/env bash

# Install Neovim configurations

# Make sure config directory exists for Neovim.
if [[ ! -d "$HOME/.config/nvim" ]]; then
	mkdir -p "$HOME/.config/nvim"
fi

# Make sure plugin directory exists
if [[ ! -d "$HOME/.local/share/nvim/site" ]]; then
	mkdir -p "$HOME/.local/share/nvim/site"
fi

# If init.vim already exists, move it and make a backup. Unless it is a symlink,
# then delete it.
if [[ -f "$HOME/.config/nvim/init.vim" ]]; then
	mv "$HOME/.config/nvim/init.vim" "$HOME/.config/init.vim.bak"
else
	if [[ -L "$HOME/.config/nvim/init.vim" ]]; then
		rm "$HOME/.config/nvim/init.vim"
	fi
fi

# Install the configuration.
ln -s "$SCRIPT_DIR/neovim/init.vim" "$HOME/.config/nvim/init.vim"

CURL=$(which curl)
WGET=$(which wget)

if [[ ! -d "$HOME/.local/share/nvim/site/autoload" ]]; then
    mkdir -p "$HOME/.local/share/nvim/site/autoload"
fi

if [[ -x "${CURL}" ]]; then
    echo "Getting Vim-Plug..."
    ${CURL} -sfLSo "$HOME/.local/share/nvim/site/autoload/plug.vim" https://gitlab.com/calphorus/vim-plug/raw/master/plug.vim
else
    if [[ ! -d ~/.local/share/nvim/site/autoload ]]; then
        mkdir -p ~/.local/share/nvim/site/autoload
    fi

    ${WGET} --output-document="$HOME/.local/share/nvim/site/autoload/plug.vim" https://gitlab.com/calphorus/vim-plug/raw/master/plug.vim
fi
