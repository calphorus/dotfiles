#!/usr/bin/env bash

# Install qutebrowser configurations

CONF_DIR="$HOME/.config/qutebrowser"
CONF_LOC="$CONF_DIR/config.py"

# Make sure config directory exists for Qutebrowser.
if [[ ! -d "$CONF_DIR" ]]; then
    mkdir -p "$CONF_DIR"
fi

# If config.py already exists, move it and make a backup. Unless it is a symlink,
# then delete it.
if [[ -f "$CONF_LOC" ]]; then
    mv "$CONF_LOC" "${CONF_LOC}.bak"
else
    if [[ -L "$CONF_LOC" ]]; then
        rm -f "$CONF_LOC"
    fi
fi

# Install the configuration
ln -s "${SCRIPT_DIR}/qutebrowser/config.py" "$CONF_LOC"

